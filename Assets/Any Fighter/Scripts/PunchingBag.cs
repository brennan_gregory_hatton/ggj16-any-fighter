﻿using UnityEngine;
using System.Collections;

public class PunchingBag : MonoBehaviour {
	
	Vector3 startAngle;
	
	float angle;
	float velocity;
	float drag = 01f;
	float gravity = 0.9f;
	float mass = 10f;
	
	// Use this for initialization
	void Start () {
		velocity = 0;
		startAngle = this.transform.eulerAngles;
		
		angle = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
		//apply drag
		if (velocity > 0)
			velocity = Mathf.Max(velocity - drag*Time.deltaTime, 0);
		
		else if (velocity > 0)
			velocity = Mathf.Min(velocity + drag*Time.deltaTime, 0);
		
		//apply gravity
		if (angle > 0)
			velocity -= gravity*mass*Mathf.Abs(angle)*Time.deltaTime;
		
		else if (angle < 0)
			velocity += gravity*mass*Mathf.Abs(angle)*Time.deltaTime;
		
		angle += velocity*Time.deltaTime;
		
		this.transform.rotation = Quaternion.EulerAngles(startAngle.x+angle,startAngle.y,startAngle.z);
	}
	
	public void Hit(float force)
	{
		velocity = force / mass;
	}
}
