﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class GUIManager : MonoBehaviour {
	
	float healthBarMaxLength;
	const float attackBarMaxLength = 25;
	
	public GameObject HealthBarObj1, HealthBarObj2;
	public Renderer HealthBarRen1, HealthBarRen2;
	
	
	List<GameObject> P1AttackObj = new List<GameObject>();
	List<GameObject> P2AttackObj = new List<GameObject>();
	
	public Color HealthBarMaxColor, HealthBarMinColor, AttackBarColor;
	
	public GameObject screenSpace;
	public GameObject Title;
	public GameObject Logo;
	public GameObject damangeNotificationPrefab;
	public AttackUI p1AttackUI, p2AttackUI;
	public Text p1KeyToJoin, p2KeyToJoin, startTimer;
	string startTimerText = "Starting Game in ";
	
	// Use this for initialization
	void Start () {
		healthBarMaxLength = Screen.width/2 * 10;
		UpdateHealth(100, true);
		UpdateHealth(100, false);
		Menu(true);
	}
	
	
	
	public void CreateDamageNotification(int damage,bool atP1)
	{
		GameObject notification = (GameObject)Instantiate(damangeNotificationPrefab);
		
		notification.transform.SetParent(screenSpace.transform);
		
		notification.GetComponent<PopUp>().Init(damage.ToString(),atP1,0.7f + damage/20);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public void UpdateHealth(float percent, bool p1)
	{
		if (p1)
		{
			
			HealthBarObj1.transform.localScale = new Vector3(percent/100 * healthBarMaxLength,2f,0.1f);
			HealthBarRen1.material.color = Color.LerpUnclamped(HealthBarMinColor,HealthBarMaxColor,percent/100);
		}
		else{
			HealthBarObj2.transform.localScale = new Vector3(percent/100 * healthBarMaxLength,2f,0.1f);
			HealthBarRen2.material.color = Color.LerpUnclamped(HealthBarMinColor,HealthBarMaxColor,(float)(percent/100));
			
		}
	}
	
	
	public void UpdateAttack(int power, int id, bool p1)
	{
		if (p1)
		{
			GameManager.instance.ui.p1AttackUI.UpdateAttack(id,power);
		}
		else
		{
			GameManager.instance.ui.p2AttackUI.UpdateAttack(id,power);
		}
	}
	
	public void Menu(bool on)
	{
		Title.gameObject.SetActive(on);
		Logo.gameObject.SetActive(!on);
		HealthBarObj1.gameObject.SetActive(!on);
		HealthBarObj2.gameObject.SetActive(!on);
		
		if(on)
		{
			
			p1KeyToJoin.text = p1KeyToJoin.text.Replace("#",GameManager.instance.player1.AttackKey[0].ToString().Replace("Arrow",""));
			p2KeyToJoin.text = p2KeyToJoin.text.Replace("#",GameManager.instance.player2.AttackKey[0].ToString().Replace("Arrow",""));
		}
		
		UpdatePlayerJoinText();
	}
	
	public void StartGameTimer()
	{
		startTimer.gameObject.transform.parent.gameObject.SetActive(true);
	}
	
	public void UdpateWaitTimer(float time)
	{
		startTimer.text = startTimerText + ((int)time).ToString();
	}
	
	public void UpdatePlayerJoinText()
	{
		
		
		//If both players active
		if (GameManager.instance.player1.gameObject.active && GameManager.instance.player2.gameObject.active)
		{
			startTimer.gameObject.transform.parent.gameObject.SetActive(false);
			p1KeyToJoin.gameObject.SetActive(false);
			p2KeyToJoin.gameObject.SetActive(false);
		}
		//if player 1 is active
		else if (GameManager.instance.player1.gameObject.active)
		{
			
			startTimer.gameObject.transform.parent.gameObject.SetActive(true);
			p1KeyToJoin.gameObject.SetActive(false);
			p2KeyToJoin.gameObject.SetActive(true);
		}
		//if player 2 is active
		else if (GameManager.instance.player2.gameObject.active)
		{
			
			startTimer.gameObject.transform.parent.gameObject.SetActive(true);
			p2KeyToJoin.gameObject.SetActive(false);
			p1KeyToJoin.gameObject.SetActive(true);
		}
		//if none are active\
		else{
			
			startTimer.gameObject.transform.parent.gameObject.SetActive(false);
			p2KeyToJoin.gameObject.SetActive(true);
			p1KeyToJoin.gameObject.SetActive(true);
		}
	}
}
