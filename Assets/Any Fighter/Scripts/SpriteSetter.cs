﻿using UnityEngine;
using System.Collections;

public class SpriteSetter : MonoBehaviour {

	public Sprite[] array;
	
	SpriteRenderer spriteRen;
	
	void Start () {
		if (spriteRen == null)
			spriteRen = GetComponent<SpriteRenderer>();
	}
	
	public void SetSprite(int i)
	{
		if (spriteRen == null)
		{
			spriteRen = GetComponent<SpriteRenderer>();
		}
		
		spriteRen.sprite = array[i];
	}
}
