﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AttackUI : MonoBehaviour {
	
	public GameObject Punch, Kick, Hook, blockObj; 
	Text PunchKey, KickKey, HookKey, blockKey; 
	SpriteSetter PunchPower, KickPower, HookPower; 
	
	float timerPunch = 0, timerKick = 0, timerHook = 0, timerBlock = 0;
	
	const float timeHighlighted = 0.5f;
	
	void Start()
	{
		PunchPower = Punch.transform.parent.FindChild("powerUpSprite_0").GetComponent<SpriteSetter>();
		KickPower = Kick.transform.parent.FindChild("powerUpSprite_0").GetComponent<SpriteSetter>();
		HookPower = Hook.transform.parent.FindChild("powerUpSprite_0").GetComponent<SpriteSetter>();
		
		PunchKey = Punch.transform.parent.FindChild("CanvasParent").GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
		KickKey = Kick.transform.parent.FindChild("CanvasParent").GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
		HookKey = Hook.transform.parent.FindChild("CanvasParent").GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
		blockKey = blockObj.transform.parent.FindChild("CanvasParent").GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
		
	}
	
	public void SetKey(int id, string key)
	{
		if (key.Length > 6)
		{
			if (key.Contains("Arrow"))
			{
				
				key = key.Replace("Arrow","");
				
			}
			else
				key.Remove(6,key.Length-6);
		}
		switch(id)
		{
			
			//punck
		case 0: 
			PunchKey.text = key;
			break;
			
			//kick
		case 1:
			KickKey.text = key;
			break;
			
			//hook
		case 2:
			HookKey.text = key;
			break;
		default:
			
			blockKey.text = key;
			break;
		}
	}
	
	public void Block()
	{
		blockObj.SetActive(true);
		timerBlock = 0;
	}
	
	public void Attack(int i)
	{
		switch(i)
		{
			
			//punck
		case 0: 
			timerPunch = 0;
			Punch.gameObject.SetActive(true);
			break;
			
			//kick
		case 1:
			timerKick = 0;
			Kick.gameObject.SetActive(true);
			break;
			
			//hook
		case 2:
			timerHook = 0;
			Hook.gameObject.SetActive(true);
			break;
		}
	}
	
	public void UpdateAttack(int i, int power)
	{
		switch(i)
		{
			
			//punck
		case 0: 
			
			PunchPower.SetSprite(power);
			break;
			
			//kick
		case 1:
			KickPower.SetSprite(power);
			break;
			
			//hook
		case 2:
			HookPower.SetSprite(power);
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	 if (timerPunch > timeHighlighted)
	 {
	 	Punch.gameObject.SetActive(false);
	 }else
		 timerPunch += Time.deltaTime;
		
	 if (timerKick > timeHighlighted)
	 {
	 	Kick.gameObject.SetActive(false);
	 }else
		 timerKick += Time.deltaTime;
	 
	 if (timerHook > timeHighlighted)
	 {
	 	Hook.gameObject.SetActive(false);
	 }else
		 timerHook += Time.deltaTime;
	 
	 if (timerBlock > timeHighlighted)
	 {
	 	blockObj.gameObject.SetActive(false);
	 }else
		 timerBlock += Time.deltaTime;
	 
	 
	}
}
