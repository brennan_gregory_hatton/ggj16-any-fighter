﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class characterAudioManager : MonoBehaviour {
	
	public List<AudioClip> WinFX = new List<AudioClip>();
	public List<AudioClip> LoseFX = new List<AudioClip>();
	public List<AudioClip> PunchFX = new List<AudioClip>();
	public List<AudioClip> KickFX = new List<AudioClip>();
	public List<AudioClip> GetPunchedFX = new List<AudioClip>();
	public List<AudioClip> GetKickedFX = new List<AudioClip>();
	
	int id;
	
	
	public void PlayerPunchFX()
	{
		if (PunchFX.Count == 0)
			return;
		
		id = Random.Range(0,PunchFX.Count);
		
		AudioSource.PlayClipAtPoint(PunchFX[id],Camera.main.transform.position);
	}
	
	public void PlayerWinFX()
	{
		if (WinFX.Count == 0)
			return;
		
		id = Random.Range(0,WinFX.Count);
		
		AudioSource.PlayClipAtPoint(WinFX[id],Camera.main.transform.position);
	}
	
	public void PlayerLoseFX()
	{
		if (LoseFX.Count == 0)
			return;
		
		id = Random.Range(0,LoseFX.Count);
		
		AudioSource.PlayClipAtPoint(LoseFX[id],Camera.main.transform.position);
	}
	
	public void PlayerKickFX()
	{
		if (KickFX.Count == 0)
			return;
		
		id = Random.Range(0,KickFX.Count);
		
		AudioSource.PlayClipAtPoint(KickFX[id],Camera.main.transform.position);
	}
	
	public void PlayerGetPunchedFX()
	{
		if (GetPunchedFX.Count == 0)
			return;
		
		id = Random.Range(0,GetPunchedFX.Count);
		
		AudioSource.PlayClipAtPoint(GetPunchedFX[id],Camera.main.transform.position);
	}
	
	public void PlayerGetKickedFX()
	{
		if (GetKickedFX.Count == 0)
			return;
		
		id = Random.Range(0,GetKickedFX.Count);
		
		AudioSource.PlayClipAtPoint(GetKickedFX[id],Camera.main.transform.position);
	}
}
