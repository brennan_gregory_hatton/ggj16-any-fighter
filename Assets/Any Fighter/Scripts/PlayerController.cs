﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlayerController : MonoBehaviour {
	
	
	//Set keys for controls in inspector
	//Constants 
	public static readonly int attacks = 3;
	public const int maxHealth = 100;
	const string idleString = "Idle";
	const string comboingString = "Comboing";
	const string deadString = "Death";
	const string winString = "Victory";
	const string resetString = "Reset";
	public static readonly string[] attackString = new string[] {"Punch","Kick", "Hook"};
	static readonly string[] blockString = new string[] {"PunchBlock","KickBlock", "HookBlock"};
	static readonly string[] hitString = new string[] {"FaceHit","stomachHit", "FaceHit"};
	static readonly float[] defaultMutliplyers = new float[] {1f, 3f, 2f};
	static readonly short[] attackMin = new short[] {1, 3, 2};
	
	
	const int combos = 6;
	const int comboMaxLength = 5;
	
	const float comboTrainingTimeGap = 1f;
	
	public KeyCode BlockKey;
	public KeyCode[] AttackKey = new KeyCode[attacks];
	public KeyCode[] ComboKey = new KeyCode[combos];
	
	//Position
	public Vector3 FightPosition, TrainingPosition;
	
	int comboID;
	int stageInCombo;
	bool attackDone = true;
	
	//AIConttrols
	public bool AI;
	const int AIBlockChance = 35;
	const int AIAttackFrequency = 5;
	float  AIAttackTimer;
	
	//Stats
	int[] blockPower = new int[attacks];
	int[] attackPower = new int[attacks];
	List<int> [] saveCombos = new List<int>[combos];
	int [] comboPower = new int[combos];
	
	//Vitals
	public int health = maxHealth;
	
	//Self Reference
	characterAudioManager audio;
	
	
	//Reference to opponent
	PlayerController opponent;
	public PunchingBag bag;
	public int playerID;
	
	//End Game
	bool gameWon = true;
	float winTimer = 0, winDelay = 1f;
	
	//Link to animator of character
	public Animator animator;
	
	//training
	float timer = 0;
	int comboTrainingIndex = 0, comboMoveTrainingIndex = 0;
	List<int> currentCombo = new List<int>();
	
	// Use this for initialization
	void Start () {
		
		//Check attacks are properly defined
		if (attacks != attackString.Length && attacks != blockString.Length)
			Debug.LogError("Attack or Block string does not equal defined number of attacks");
		
		
		audio = this.gameObject.GetComponent<characterAudioManager>();
		
		ResetPlayer();
	}
	
	public void ResetPlayer()
	{
		AI = false;
		gameWon = false;
		
		animator.SetTrigger(resetString);
		
		this.transform.localPosition = FightPosition;
		
		//initliaize power values
		for(int i = 0; i < attacks; i++)
		{
			blockPower[i] = 1;
			attackPower[i] = 1;
			GameManager.instance.ui.UpdateAttack(attackPower[i],i,(playerID == 0));
			
			if (playerID == 0)
				GameManager.instance.ui.p1AttackUI.SetKey(i,AttackKey[i].ToString());
			else
				GameManager.instance.ui.p2AttackUI.SetKey(i,AttackKey[i].ToString());
		}
		if (playerID == 0)
			GameManager.instance.ui.p1AttackUI.SetKey(-1,BlockKey.ToString());
		else
			GameManager.instance.ui.p2AttackUI.SetKey(-1,BlockKey.ToString());
		
		
		
		saveCombos = new List<int>[combos];
		comboPower = new int[combos];
		
		attackDone = true;
		
		//Set Animation
		animator.SetBool(comboingString,false);
		
		
		//reset health
		health = maxHealth;
		GameManager.instance.ui.UpdateHealth(health/maxHealth,(playerID == 0));
	}
	
	void Update()
	{
		switch(GameManager.instance.gameMode)
		{
			
		case GameMode.Training:
			if (AI)
				AITrain();
			else
				TrainingControls();
			break;
			
		case GameMode.Fighting:
			if (AI)
				AIFight();
			else
				fightingControls();
			
			
			if(gameWon)
			{
				
				if(winTimer <= 0)
				{
					PlayWin();
				}else
					winTimer -= Time.deltaTime;
			}
			break;
			
		}
	}
	
	//Training Controls
	void TrainingControls()
	{
		if (currentCombo == null)
		{
			currentCombo = new List<int>();
		}
		else	
		//if timer is longer than pause
		if (timer > comboTrainingTimeGap)
		{
			//if combo has been entered
			if (currentCombo.Count > 1)
			{
				
				SaveCombo();
				
			}else if (currentCombo.Count > 0)
			{
				//start new combo
				currentCombo = new List<int>();
			}
			
			
			
		}
		
		
		//Record Controls
		//Check Block
		//Check for blocking
		if (Input.GetKeyDown(BlockKey))
		{
			Block();
			
					//currentCombo.Add(-(i+1));
			
					//Increase power of attack
			//blockPower[i]++;
			
					//Finish timer
			timer = comboTrainingTimeGap;
		}
		else
		{
			//Check all attack keys
			for(int i = 0; i < attacks; i++)
			{
				//Check if button is pressed
				if (Input.GetKeyDown(AttackKey[i]) && attackDone)
				{
				
					Attack(i);
					
					currentCombo.Add(i);
					
					
					//increase power of attack
					attackPower[i]++;
					
					//Update UI
					GameManager.instance.ui.UpdateAttack(attackPower[i],i,(playerID == 0));
					
					//If combo has reached max
					if (currentCombo.Count >= comboMaxLength)
					{
						SaveCombo();
					}
						
					//reset timer
					timer = 0;
					
					
					//Exit loop
						i = attacks;
				
				}
			}
		}
		//If attack is done
		if (attackDone)
			//increase timer
			timer += Time.deltaTime;
		
	}
	
	void SaveCombo()
	{
		//Check if combo exists
		int comboID = CheckComboExists(currentCombo);
		if (comboID >= 0)
		{
					//increase combo power
			comboPower[comboID]++;
		}
				//else if free combo slots
		else if (comboTrainingIndex < combos)
		{
			
					//Check if combo is defined
			if (saveCombos[comboTrainingIndex] == null)
			{
				saveCombos[comboTrainingIndex] = new List<int>();
			}
			
					//Save combo
			saveCombos[comboTrainingIndex].InsertRange(0,currentCombo);
			
					//start new combo
			comboTrainingIndex++;
		}
				//start new combo
		currentCombo = new List<int>();
	}
	
	int CheckComboExists(List<int> combo)
	{
		bool found;
		
		//for all combos
		for(int i = 0; i < combos; i++)
		{
			//Check if combo is defined
			if (saveCombos[i] == null)
				continue;
			
			//Check for match
			if(saveCombos[i].Count == combo.Count)
			{
				found = true;
				
				//Check each move
				for(int c = 0; c < saveCombos[i].Count; c++)
				{
					if (saveCombos[i][c] != combo[c])
					{
						//not the same
						found = false;
						
						//exit loop
						c =  saveCombos[i].Count;
					}
				}
				
				if (found)
					//found
					return i;
			}
		}
		
		//not found
		return -1;
	}
	
	void AIFight()
	{
		
		if (animator.GetBool(comboingString))
		{
			//Bugged out
			if (animator.GetCurrentAnimatorStateInfo(0).IsName(idleString))
			{
				Debug.LogWarning("Combo bug still exists");
				attackDone = true;
				PerformCombo(comboID);
			}
			
			return;
		}
		
		//Check if blocking
		if (AIBlock())
		{
			Block();
		}else
		{
			AIAttack();
		}
	}
	
	
	bool AIBlock()
	{
		//Check if opponenet is attacking
		for(int i = 0; i < attacks; i++)
		{
			//Check which attack is being used
			if (opponent.animator.GetCurrentAnimatorStateInfo(0).IsName(attackString[i]))
			{
				if (UnityEngine.Random.Range(0,100) >= AIBlockChance)
				{
					return true;
				}
				
				return false;
			}
		}
		
		return false;
	}
	
	void AIAttack()
	{
		if (AIAttackTimer <= 0 && attackDone)
		{
			AIAttackTimer = UnityEngine.Random.Range(0f,(float)AIAttackFrequency);
			
			Attack(UnityEngine.Random.Range(0,attacks));
		}else
			AIAttackTimer-= Time.deltaTime;
		
	}
	
	void AITrain()
	{
		if (animator.GetBool(comboingString))
			return;
		
		if (currentCombo == null)
		{
			currentCombo = new List<int>();
		}
		else	
		//if timer is longer than pause
		if (timer > comboTrainingTimeGap)
		{
		//if combo has been entered
			if (currentCombo.Count > 1)
			{
				
				SaveCombo();
				
			}else if (currentCombo.Count > 0)
			{
			//start new combo
				currentCombo = new List<int>();
			}
			
			
			
		}
		
		if (AIAttackTimer <= 0 && attackDone)
		{
			AIAttackTimer = UnityEngine.Random.Range(0f,(float)AIAttackFrequency/3);
			
			int i = UnityEngine.Random.Range(0,attacks);
			Attack(i);
			
			currentCombo.Add(i);
			
			
					//increase power of attack
			attackPower[i]++;
			
					//Update UI
			GameManager.instance.ui.UpdateAttack(attackPower[i],i,(playerID == 0));
			
					//If combo has reached max
			if (currentCombo.Count >= comboMaxLength)
			{
				SaveCombo();
			}
			
					//reset timer
			timer = 0;
		}else
			AIAttackTimer-= Time.deltaTime;
		
	}
	
	//Fighting Controls
	void fightingControls()
	{
		if (animator.GetBool(comboingString))
		{
			//Bugged out
			if (animator.GetCurrentAnimatorStateInfo(0).IsName(idleString))
			{
				Debug.LogWarning("Combo bug still exists");
				attackDone = true;
				PerformCombo(comboID);
			}
			
			return;
		}
		
		//Check if blocking
		if (Input.GetKeyDown(BlockKey))
		{
			Block();
		}else
		{
			//Check all attack keys
			for(int i = 0; i < attacks; i++)
			{
				//Check if button is pressed
				if (Input.GetKeyDown(AttackKey[i]))
				{
					
					//Perform attack
					Attack(i);
					
					//Exit loop
					i = attacks;
				}
			}
		}
		
		
	}
	
	void StartCombo(int i )
	{
		resetTriggers();
		
		if (saveCombos[i] == null || saveCombos[i].Count == 0)
		{
			animator.SetBool(comboingString,false);
			
			Attack(0);
			
			return;
		}
		
		comboID = i;
		animator.SetBool(comboingString,true);
		
		stageInCombo = 0;
		
		PerformCombo(comboID);
	}
	
	void PerformCombo(int i )
	{
		resetTriggers();
		
		//if ready for next move
		if (attackDone == true)
		{
			Attack(saveCombos[i][stageInCombo]);
			
			stageInCombo++;
			
			if (stageInCombo >= saveCombos[i].Count)
			{
				EndCombo();
			}
		}
		
	}
	
	void EndCombo()
	{
		animator.SetBool(comboingString,false);
	}
	
	//Block
	void Block()
	{
		attackDone = true;
		resetTriggers();
		
		//Trigger Animator
		for(int i = 0; i < attacks; i++)
		{
			//Check which attack is being used
			if (opponent.animator.GetCurrentAnimatorStateInfo(0).IsName(attackString[i]))
			{
				animator.SetTrigger(blockString[i]);
				return;
			}
		}
		
		animator.SetTrigger(blockString[0]);
		
		//Update Attack Buttons
		if(playerID == 0)
			GameManager.instance.ui.p1AttackUI.Block();
		else
			GameManager.instance.ui.p2AttackUI.Block();
	}
	
	void Attack(int i)
	{
		attackDone = false;
		
		resetTriggers();
		
		//Trigger Animator
		animator.SetTrigger(attackString[i]);
		
		//Update Attack Buttons
		if(playerID == 0)
			GameManager.instance.ui.p1AttackUI.Attack(i);
		else
			GameManager.instance.ui.p2AttackUI.Attack(i);
		
		//Update UI
		//attackPower[i]*defaultMutliplyers[i]
	}
	
	void resetTriggers()
	{
		for(int c = 0; c < attacks; c++)
		{
			animator.ResetTrigger(attackString[c]);
			animator.ResetTrigger(blockString[c]);
		}
	}
	
	
	//<summary>
	//Call this at end of an attack animation
	//</summary>
	//This is called in the animation
	public void CheckHitOpponent()
	{
		//Set Attack done
		attackDone = true;
		
		//Check if in training
		if (GameManager.instance.gameMode == GameMode.Training)
		{
			for(int i = 0; i < attacks; i++)
			{
				//Check which attack is being used
				if (this.animator.GetCurrentAnimatorStateInfo(0).IsName(attackString[i]))
				{
					GameManager.instance.audio.BagHit();
					bag.Hit((attackPower[i]*defaultMutliplyers[i])+attackMin[i]);
				}
			}
			return;
		}
		
		for(int i = 0; i < attacks; i++)
		{
			//Check which attack is being used
			if (this.animator.GetCurrentAnimatorStateInfo(0).IsName(attackString[i]))
			{
				
				//Check if opponent was blocking right
				if (opponent.animator.GetCurrentAnimatorStateInfo(0).IsName(blockString[i]))
				{
    				//Attack was blocked
					//End combo
					EndCombo();
					opponent.StartCombo(0);
				}
				//Check to make sure opponent isnt in combo mode
				else if (!opponent.animator.GetBool(comboingString) ||
					//unless you are comboing!
				animator.GetBool(comboingString))
				{
					
					//Damage opponent
					opponent.TakeDamange((int)(attackPower[i]*defaultMutliplyers[i])+attackMin[i], i);
					
					
					//end loop
					i = attackString.Length;
					
					//If comboing
					if (animator.GetBool(comboingString))
						//Contiune Combo
						PerformCombo(comboID);
					
				}
				
				
			}
		}
	}
	
	public void TakeDamange(int damage, int id)
	{
		if (health <= 0)
			return;
		
		
		//Stop attacking / blocking
		resetTriggers();
		
		if (damage > 0)
		{
			
			health -= damage;
			
		//PopUp
			GameManager.instance.ui.CreateDamageNotification(damage,(playerID == 0));
		}
		
		//Check if dead
		if (health <= 0)
		{
			//dead
			//die
			animator.SetTrigger(deadString);
			audio.PlayerLoseFX();
			
			//Notifiy opponent won
			opponent.Win();
			
		}else if (damage > 0)
		{
			//injujred
			//flinch
			animator.SetTrigger(hitString[id]);
			
			//grown
			if (id == 1)
				audio.PlayerGetKickedFX();
			else
				audio.PlayerGetPunchedFX();
		}
		
		GameManager.instance.ui.UpdateHealth((float)health/(float)maxHealth,(playerID == 0));
		
	}
	
	
	public void Win()
	{
		//Check if both lost
		if (health <= 0)
		{
			
			animator.SetTrigger(deadString);
			opponent.animator.SetTrigger(deadString);
			
			GameManager.instance.audio.Draw();
			gameWon = false;
			opponent.gameWon = false;
			
		}else
		{
			gameWon = true;
			winTimer = winDelay;
		}
			
		
	}
	
	public void PlayWin()
	{
		gameWon = false;
		//We won!
		animator.SetTrigger(winString);
		audio.PlayerWinFX();
		
		GameManager.instance.GameOver((playerID == 0));
	}
	
	
	public void SetOpponent(PlayerController opp)
	{
		opponent = opp;
	}
	
}
