﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopUp : MonoBehaviour {
	
	public float life = 1f;
	public float scaleFactor = 2;
	
	Vector3 startScale;
	float timeAlive = 0;
	// Use this for initialization
	void Start () {
		Destroy(this.gameObject,life);
	}
	
	public void Init(string _text, bool atP1, float scale)
	{
		Vector3 position;
		
		if (atP1)
		{
			position.x = GameManager.instance.player1.transform.position.x;
			position.y = GameManager.instance.player1.transform.position.y;
			position.z = GameManager.instance.player1.transform.position.z;
		}
		else
		{
			position.x = GameManager.instance.player2.transform.position.x;
			position.y = GameManager.instance.player2.transform.position.y;
			position.z = GameManager.instance.player2.transform.position.z;
		}
		
		position.y += 0.1f;
		position.z -= 0.1f;
		
		this.transform.position = position;
		
		startScale = this.transform.localScale * scale;
		
		this.transform.localScale = startScale* 2 / 3;
		
		this.transform.GetChild(0).gameObject.GetComponent<Text>().text = _text;
	}
	
	// Update is called once per frame
	void Update () {
		timeAlive += Time.deltaTime;
		
		if (timeAlive <= life/2)
		{
			this.transform.localScale = startScale * (timeAlive/(life/2)) * 4 / 3;
			
		}else
		{
			
			this.transform.localScale = startScale * ((life/2 - (timeAlive-life/2 )/2)/(life/2)) * 4 / 3;
		}
	}
}
