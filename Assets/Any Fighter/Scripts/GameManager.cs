﻿using UnityEngine;
using System.Collections;

public enum GameMode{
	Menu = -1,
	Training = 0,
	Fighting = 1,
	End = 2
}

public class GameManager : MonoBehaviour {
	
	
	public static GameManager instance = null;
	
	[HideInInspector]
	public GameMode gameMode;
	
	bool debugMode = false;
	
	public PlayerController player1, player2;
	
	public float gameStartWaitTime = 2;
	public float waitFor2ndPlayerTime = 5;
	public float TrainingTime = 15;
	public float FightingTime = 10;
	public float EndTime = 5;
	
	public audioManager audio;
	public GUIManager ui;
	public Camera enviromentCamera;
	[SerializeField]
	public Transform enviromentCameraTrainTrans, enviromentCameraFightTrans;
	
	float gameStateTimer;
	
	SpriteRenderer player1SpriteRenderer, player2SpriteRenderer;
	
	void Awake()
	{
		
		if (instance != null)
			DestroyImmediate(this);
		
		instance = this;
		
	}
	
	// Use this for initialization
	void Start () {
		
		
		//Set opponenets
		player1.SetOpponent(player2);
		player2.SetOpponent(player1);
		player1.playerID = 0;
		player2.playerID = 1;
		
		player1SpriteRenderer = player1.GetComponent<SpriteRenderer>();
		player2SpriteRenderer = player2.GetComponent<SpriteRenderer>();
		
		player1.gameObject.SetActive(false);
		player2.gameObject.SetActive(false);
		
		gameMode = GameMode.Menu;
		
		audio = this.gameObject.GetComponent<audioManager>();
		
		
		audio.GameStartTitleScreen();
		ui.UpdatePlayerJoinText();
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (debugMode)
			debugControls();
		
		switch(gameMode)
		{
		case GameMode.Menu:
			
			GameStateMenu();
			
			if (Input.anyKeyDown)
			{
				ui.UpdatePlayerJoinText();
					
			}
				break;
			case GameMode.Training:
				
				gameStateTimer += Time.deltaTime;
				if (gameStateTimer > TrainingTime)
				{
					NextState();
				}
					
				break;
			case GameMode.Fighting:
				
				
				gameStateTimer += Time.deltaTime;
				if (gameStateTimer > FightingTime)
				{
					NextState();
				}
				break;
			case GameMode.End:
				
				
				gameStateTimer += Time.deltaTime;
				if (gameStateTimer > EndTime && Input.anyKey)
				{
					NextState();
				}
				break;
		}
		
		//quit to meny
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (gameMode == GameMode.Menu)
				Application.Quit();
			
			gameMode = GameMode.Menu;
		}
	}
	
	void debugControls()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			
			gameStateTimer = 0;
			
			switch(gameMode)
			{
				
			case GameMode.Menu:
				
				NextState();
				break;
				
			case GameMode.Training:
				
				NextState();
				break;
				
			case GameMode.Fighting:
				
				NextState();
				break;
				
			case GameMode.End:
				
				NextState();
				break;
				
			}
		}
	}
	
	void NextState()
	{
		switch(gameMode)
		{
		case GameMode.Menu:
			
			audio.TrainingStart();
			ui.Menu(false);
			
			gameStateTimer = 0;
			
			player1.gameObject.SetActive(true);
			player2.gameObject.SetActive(true);
			
			//face away from each other
			PlayersSetTraining();
			
			gameMode = GameMode.Training;
			break;
			
		case GameMode.Training:
			
			audio.GameStart();
			gameStateTimer = 0;
			PlayersSetFight();
			gameMode = GameMode.Fighting;
			break;
			
		case GameMode.Fighting:
			
			gameStateTimer = 0;
			gameMode = GameMode.End;
			break;
			
		case GameMode.End:
			
			gameStateTimer = 0;
			player1.ResetPlayer();
			player2.ResetPlayer();
			player1.gameObject.SetActive(false);
			player2.gameObject.SetActive(false);
			gameMode = GameMode.Menu;
			ui.Menu(true);
			
			break;
		}
	}
	
	void GameStateMenu()
	{
		
		//Check if players have joined
		if (player1.gameObject.active && player2.gameObject.active)
		{
			//timer to start game
			gameStateTimer+= Time.deltaTime;
			
			//If timer is up
			if (gameStateTimer >= gameStartWaitTime)
			{
				//next game state
				NextState();
			}
		}
		else{
			
			//Check keys pressed to join game
			JoinGameKeyPress();
			
			//Check if one player has joined
			if (player1.gameObject.active || player2.gameObject.active)
			{
				gameStateTimer+= Time.deltaTime;
				ui.UdpateWaitTimer(waitFor2ndPlayerTime - gameStateTimer);
			}
			
			//If timer is up
			if (gameStateTimer >= waitFor2ndPlayerTime)
			{
				if (player1.gameObject.active )
				{
					player2.gameObject.SetActive(true);
					player2.AI = true;
				}else if (player2.gameObject.active )
				{
					player1.gameObject.SetActive(true);
					player1.AI = true;
				}
				//TODO set AI player
				//next game state
				NextState();
			}
			
			
		}
	}
	
	void PlayersSetTraining()
	{
		ui.UpdateHealth(100, true);
		ui.UpdateHealth(100, false);
		
		player1SpriteRenderer.flipX = true;
		player2SpriteRenderer.flipX = false;
		
		player1.gameObject.transform.localPosition = player1.TrainingPosition;
		player2.gameObject.transform.localPosition = player2.TrainingPosition;
		
		//UpdateCamera
		
		enviromentCamera.transform.position = enviromentCameraTrainTrans.position;
		enviromentCamera.transform.rotation = enviromentCameraTrainTrans.rotation;
	}
	
	void PlayersSetFight()
	{
		
		player1SpriteRenderer.flipX = false;
		player2SpriteRenderer.flipX = true;
		
		player1.gameObject.transform.localPosition = player1.FightPosition;
		player2.gameObject.transform.localPosition = player2.FightPosition;
		//UpdateCamera
		enviromentCamera.transform.position = enviromentCameraFightTrans.position;
		enviromentCamera.transform.rotation = enviromentCameraFightTrans.rotation;
	}
	
	void FlipPlayers()
	{
		player1SpriteRenderer.flipX = !player1SpriteRenderer.flipX ;
		player2SpriteRenderer.flipX = !player2SpriteRenderer.flipX ;
	}
	
	void JoinGameKeyPress()
	{
		
		if (Input.GetKeyDown(player1.AttackKey[0]))
		{
			gameStateTimer = 0;
			audio.Player1Joins();
			player1.gameObject.SetActive(true);
			ui.UpdatePlayerJoinText();
			player1.ResetPlayer();
		}
		
		if (Input.GetKeyDown(player2.AttackKey[0]))
		{
			gameStateTimer = 0;
			audio.Player2Joins();
			player2.gameObject.SetActive(true);
			ui.UpdatePlayerJoinText();
			player2.ResetPlayer();
		}
	}
	
	public void GameOver(bool p1Wins)
	{
		audio.GameOver(p1Wins);
		
		NextState();
	}
	
	//<summary>
	//
	//</summary>
	void OnGUI()
	{
		GUI.Label(new Rect(0 ,0 ,400 ,20 ),"Game State "+ gameMode.ToString());
		
		GUI.color = Color.black;
		switch(gameMode)
		{
		case GameMode.Menu:
			
			if (player1.gameObject.active && player2.gameObject.active)
				GUI.Label(new Rect(Screen.width/2 ,50 ,400 ,20 ),"Game starts in "+ (int)(gameStartWaitTime - gameStateTimer));
			break;
			
		case GameMode.Training:
			
			GUI.Label(new Rect(Screen.width/2 ,50 ,400 ,20 ),"Days until fight: "+ (int)(TrainingTime - gameStateTimer));
			break;
			
		case GameMode.Fighting:
			
			GUI.Label(new Rect(Screen.width/2 ,50 ,400 ,20 ), ((int)(FightingTime - gameStateTimer)).ToString());
			break;
			
		case GameMode.End:
			
			break;
		}
	}
}
