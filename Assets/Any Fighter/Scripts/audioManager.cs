﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class audioManager : MonoBehaviour {
	
	public List<AudioClip> BagHitFX = new List<AudioClip>();
	public List<AudioClip> Player1WinsFX = new List<AudioClip>();
	public List<AudioClip> Player2WinsFX = new List<AudioClip>();
	public List<AudioClip> DrawFX = new List<AudioClip>();
	public List<AudioClip> WelcomePlayer1FX = new List<AudioClip>();
	public List<AudioClip> WelcomePlayer2FX = new List<AudioClip>();
	public List<AudioClip> TrainingLongFX = new List<AudioClip>();
	public List<AudioClip> TrainingShortFX = new List<AudioClip>();
	public List<AudioClip> FightFX = new List<AudioClip>();
	public List<AudioClip> TitleScreenFX = new List<AudioClip>();
	
	int id;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void BagHit()
	{
		
		if (BagHitFX.Count == 0)
			return;
		
		id = Random.Range(0,BagHitFX.Count);
		
		AudioSource.PlayClipAtPoint(BagHitFX[id],Camera.main.transform.position);
	}
	
	public void Player1Joins()
	{
		
		if (WelcomePlayer1FX.Count == 0)
			return;
		
		id = Random.Range(0,WelcomePlayer1FX.Count);
		
		AudioSource.PlayClipAtPoint(WelcomePlayer1FX[id],Camera.main.transform.position);
	}
	
	public void Player2Joins()
	{
		
		if (WelcomePlayer2FX.Count == 0)
			return;
		
		id = Random.Range(0,WelcomePlayer2FX.Count);
		
		AudioSource.PlayClipAtPoint(WelcomePlayer2FX[id],Camera.main.transform.position);
	}
	
	public void GameOver(bool p1Wins)
	{
		if (p1Wins)
		{
			if (Player1WinsFX.Count == 0)
				return;
			
			id = Random.Range(0,Player1WinsFX.Count);
			
			AudioSource.PlayClipAtPoint(Player1WinsFX[id],Camera.main.transform.position);
		}else
		{
			
			if (Player2WinsFX.Count == 0)
				return;
			
			id = Random.Range(0,Player2WinsFX.Count);
			
			AudioSource.PlayClipAtPoint(Player2WinsFX[id],Camera.main.transform.position);
		}
		
	}
	
	public void Draw()
	{
		if (DrawFX.Count == 0)
			return;
		
		id = Random.Range(0,DrawFX.Count);
		
		AudioSource.PlayClipAtPoint(DrawFX[id],Camera.main.transform.position);
	}
	
	public void GameStart()
	{
		
		if (FightFX.Count == 0)
			return;
		
		id = Random.Range(0,FightFX.Count);
		
		AudioSource.PlayClipAtPoint(FightFX[id],Camera.main.transform.position);
	}
	
	public void TrainingStart()
	{
		//TODO
		//Check if fist time player
			//player long
		//else
			//play short
		
		if (TrainingLongFX.Count == 0)
			return;
		
		id = Random.Range(0,TrainingLongFX.Count);
		
		AudioSource.PlayClipAtPoint(TrainingLongFX[id],Camera.main.transform.position);
	}
	
	public void GameStartTitleScreen()
	{
		
		if (TitleScreenFX.Count == 0)
			return;
		
		id = Random.Range(0,TitleScreenFX.Count);
		
		AudioSource.PlayClipAtPoint(TitleScreenFX[id],Camera.main.transform.position);
	}
}
